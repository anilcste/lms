package testCase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Reporter;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import junit.framework.Assert;
import projectBase.ProjecBase;
import projectListeners.ExtentListeners;

@Listeners(ExtentListeners.class)
public class testActivity13 extends ProjecBase {

	@Parameters({ "username", "password" })
	@Test
	public void testcase13(String username, String password) {

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		JavascriptExecutor js = (JavascriptExecutor) driver;

		click("//a[contains(text(),'My Account')]");
		Reporter.log("TC13- Clicking on My account page");

		click("//a[@class='ld-login ld-login ld-login-text ld-login-button ld-button']");
		Reporter.log("TC13- Clicking on login page");

		sendkey("//input[@id='user_login']", username);
		sendkey("//input[@id='user_pass']", password);
		Reporter.log("TC13- Entered UserName and PASSWORD");

		click("//input[@id='wp-submit']");
		Reporter.log("TC13- clicking on submit button");

		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[contains(text(),'All Courses')]")));
		click("//a[contains(text(),'All Courses')]");
		Reporter.log("TC13- clicking on All Courses");

		js.executeScript("window.scrollBy(0,450)");

		click("//article[@id='post-24042']//a[@class='btn btn-primary']");
		Reporter.log("TC13- clicking on Content Marketing");

		js.executeScript("window.scrollBy(0,1300)");
		click("//div[contains(text(),'Effective Writing & Promoting Your Content')]");
		Reporter.log("TC13 Clicking on Effective Writing & Promoting Your Content");

		js.executeScript("window.scrollBy(0,500)");
		click("//div[@id='learndash_post_283']//input[@class='learndash_mark_complete_button']");
		Reporter.log("TC13 Clicking on Mark Complete");

		js.executeScript("window.scrollBy(0,600)");
		click("//span[contains(text(),'Growth Hacking With Your Content')]");
		Reporter.log("TC13 Clicking on Growth Hacking With Your Content");

		js.executeScript("window.scrollBy(0,600)");
		click("//div[@id='learndash_post_289']//input[@class='learndash_mark_complete_button']");
		Reporter.log("TC13 Clicking on Mark Complete");

		js.executeScript("window.scrollBy(0,450)");
		click("//a[@class='ld-primary-color']");
		Reporter.log("TC13 Clicking on Back to Lesson");

		js.executeScript("window.scrollBy(0,600)");
		click("//span[contains(text(),'The Power Of Effective Content')]");
		Reporter.log("TC13 Clicking on The Power Of Effective Content");

		js.executeScript("window.scrollBy(0,600)");
		click("//div[@id='learndash_post_24183']//input[@class='learndash_mark_complete_button']");
		Reporter.log("TC13 Clicking on Mark Complete");

		js.executeScript("window.scrollBy(0,650)");
		click("//div[@id='learndash_post_24187']//input[@class='learndash_mark_complete_button']");
		Reporter.log("TC13 Clicking on Mark Complete");

		js.executeScript("window.scrollBy(0,1300)");
		click("//div[contains(text(),'Effective Writing & Promoting Your Content')]");
		Reporter.log("TC13 Clicking on Effective Writing & Promoting Your Content");

		String progress = driver.findElement(By.xpath("//div[@class='ld-progress-percentage ld-secondary-color']"))
				.getText();
		System.out.println(progress);
		Reporter.log("TC13 progress of course is =" + progress);
		Assert.assertEquals(progress, "100% COMPLETE");

	}

}
