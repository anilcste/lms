package testCase;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import projectBase.ProjecBase;
import projectListeners.ExtentListeners;

@Listeners(ExtentListeners.class)
public class testActivity7 extends ProjecBase {

	@Test
	public void testcase7() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		click("//a[contains(text(),'All Courses')]");

		WebElement allcourse = driver.findElement(By.xpath("//div[@class='ld-course-list-items row']"));

		List<WebElement> course = allcourse.findElements(By.xpath("//div[@class='ld_course_grid col-sm-8 col-md-4 ']//h3"));

		System.out.println("Total Number of Course = " + course.size());

		for (WebElement printcourse : course) {

			System.out.println(printcourse.getText());
		}

	}
}
