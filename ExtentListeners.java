package projectListeners;

import org.testng.ITestListener;
import org.testng.ITestResult;

import projectBase.ProjecBase;

public class ExtentListeners extends ProjecBase implements ITestListener {

	@Override
	public void onTestSuccess(ITestResult result) {
		// TODO Auto-generated method stub
		ITestListener.super.onTestSuccess(result);
	}

	@Override
	public void onTestFailure(ITestResult result) {
		// TODO Auto-generated method stub
		
		failed(result.getMethod().getMethodName());
		
		ITestListener.super.onTestFailure(result);
		
		
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub
		ITestListener.super.onTestSkipped(result);
	}

}
