package testCase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import projectBase.ProjecBase;
import projectListeners.ExtentListeners;

@Listeners(ExtentListeners.class)
public class testActivity4 extends ProjecBase{

	@Test
	public void testcase4() {
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    String highratedtitle = driver.findElement(By.xpath("//div[@class='ld-course-list-items row']/div[2]/article[1]/div[2]/h3[1]")).getText();
		String expectedtitle = "Email Marketing Strategies";
	    Assert.assertEquals(expectedtitle, highratedtitle);
	    Reporter.log("TC4- verifying course name");
		
	}
	
}
