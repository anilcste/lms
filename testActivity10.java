package testCase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import projectBase.ProjecBase;
import projectListeners.ExtentListeners;

@Listeners(ExtentListeners.class)
public class testActivity10 extends ProjecBase {
	@Parameters({ "username", "password" })
	@Test
	public void testcase10(String username, String password) {

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		JavascriptExecutor js = (JavascriptExecutor) driver;

		click("//a[contains(text(),'My Account')]");
		Reporter.log("TC10- Clicking on My account page");

		click("//a[@class='ld-login ld-login ld-login-text ld-login-button ld-button']");
		Reporter.log("TC10- Clicking on login page");

		sendkey("//input[@id='user_login']", username);
		sendkey("//input[@id='user_pass']", password);
		Reporter.log("TC10- Entered UserName and PASSWORD");

		click("//input[@id='wp-submit']");
		Reporter.log("TC10- clicking on submit button");

		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[contains(text(),'All Courses')]")));
		click("//a[contains(text(),'All Courses')]");
		Reporter.log("TC10- clicking on All Courses");

		js.executeScript("window.scrollBy(0,450)");
		click("//article[@id='post-24042']//a[@class='btn btn-primary']");
		Reporter.log("TC10- clicking on Content Marketing - Courses");

		Assert.assertEquals(driver.getTitle(), "Content Marketing � Alchemy LMS");
		Reporter.log("TC10- verifing Title of the course page");

		js.executeScript("window.scrollBy(0,1500)");
		Reporter.log("TC10- Scrolling page down");
		click("//div[contains(text(),'Effective Writing & Promoting Your Content')]");
		Reporter.log("TC10- clicking on course topic");

		js.executeScript("window.scrollBy(0,1500)");
		click("/html/body/div[2]/div/div[2]/div[2]/div/div/div[3]/div[2]/form/input[4]");

		js.executeScript("window.scrollBy(0,1500)");
		click("//div[@id='ld-table-list-item-289']");
		js.executeScript("window.scrollBy(0,1500)");
		click("//div[@id='learndash_post_289']//input[@class='learndash_mark_complete_button']");
		js.executeScript("window.scrollBy(0,1500)");
		click("//div[@class='ld-focus-header']//input[@class='learndash_mark_complete_button']");

	}
}
