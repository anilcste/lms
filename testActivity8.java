package testCase;

import java.util.concurrent.TimeUnit;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import projectBase.ProjecBase;
import projectListeners.ExtentListeners;
@Listeners(ExtentListeners.class)
public class testActivity8 extends ProjecBase {
    @Test
	public void testcase8 () {
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		 click("//a[contains(text(),'Contact')]");
		
		 js.executeScript("window.scrollBy(0,1000)");
		 
		 sendkey("//input[@id='wpforms-8-field_0']", "Anil Patidar");
		 sendkey("//input[@id='wpforms-8-field_1']", "anilcste@gmail.com");
		 sendkey("//input[@id='wpforms-8-field_3']", "LMS project Batch5");
		 sendkey("//textarea[@id='wpforms-8-field_2']", "Anil is working on LMS project");
		 click("//button[@id='wpforms-submit-8']");
		 String ExpectedMSG = "Thanks for contacting us! We will be in touch with you shortly."; 
		 String ActualMSG = driver.findElement(By.xpath("//div[@id='wpforms-confirmation-8']/p")).getText();
         System.out.println("Message is = "+ ActualMSG);
		 Assert.assertEquals(ActualMSG, ExpectedMSG);
		 
		
	}
}
