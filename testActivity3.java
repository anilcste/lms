package testCase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import projectBase.ProjecBase;
import projectListeners.ExtentListeners;

@Listeners(ExtentListeners.class)
public class testActivity3 extends ProjecBase {

	@Test
	public void testcase3 () {
    
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    WebElement firstBox =driver.findElement(By.xpath("//div[@id='uagb-column-98f62d65-f09e-42dc-9c47-a565bdf54ca0']//div[@class='uagb-column__overlay']"));
	    String Actualfistboxheading = firstBox.findElement(By.xpath("//h3[@class='uagb-ifb-title']")).getText();
	    String Expectfirstboxheading = "Actionable Training";
	    Assert.assertEquals(Actualfistboxheading, Expectfirstboxheading);
	    Reporter.log("TC3- verifying first box Heading");
	}
	
}

