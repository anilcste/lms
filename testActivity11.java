package testCase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.ClickAction;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import projectBase.ProjecBase;
import projectListeners.ExtentListeners;

@Listeners(ExtentListeners.class)
public class testActivity11 extends ProjecBase {

	@Parameters({ "username", "password" })
	@Test
	public void testcase11(String username, String password) {

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		JavascriptExecutor js = (JavascriptExecutor) driver;

		click("//a[contains(text(),'My Account')]");
		Reporter.log("TC11- Clicking on My account page");

		click("//a[@class='ld-login ld-login ld-login-text ld-login-button ld-button']");
		Reporter.log("TC11- Clicking on login page");

		sendkey("//input[@id='user_login']", username);
		sendkey("//input[@id='user_pass']", password);
		Reporter.log("TC11- Entered UserName and PASSWORD");

		click("//input[@id='wp-submit']");
		Reporter.log("TC11- clicking on submit button");

		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[contains(text(),'All Courses')]")));
		click("//a[contains(text(),'All Courses')]");
		Reporter.log("TC11- clicking on All Courses button");

		js.executeScript("window.scrollBy(0,600)");
		click("//article[@id='post-69']//a[@class='btn btn-primary']");
		Reporter.log("TC11- Click on Social Media Marketing");
		js.executeScript("window.scrollBy(0,1300)");
		Reporter.log("TC11- Scroll down on Social Media Marketing page");
		
		click("//div[@id='ld-expand-83']//span[@class='ld-text ld-primary-color'][contains(text(),'Expand')]");
		
		click("//span[contains(text(),'This is the First Topic')]");
		
		Assert.assertEquals(driver.getTitle(), "This is the First Topic � Alchemy LMS");
		
		js.executeScript("window.scrollBy(0,200)");
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='learndash_post_175']//input[@class='learndash_mark_complete_button']")));
		
		click("//div[@id='learndash_post_175']//input[@class='learndash_mark_complete_button']");
		Reporter.log("TC11- clicking on Mark Complete");
		
		js.executeScript("window.scrollBy(0,600)");
		click("//a[@class='ld-primary-color']");
		Reporter.log("TC11- clicking on back to lesson button");
		js.executeScript("window.scrollBy(0,1300)");
		
		click("//span[contains(text(),'Monitoring & Advertising')]");
		Reporter.log("TC11- clicking on Monitoring & Advertising lesson");
		js.executeScript("window.scrollBy(0,700)");
		
		click("//div[@id='learndash_post_181']//input[@class='learndash_mark_complete_button']");
		Reporter.log("TC11- clicking on Mark Complete");
		js.executeScript("window.scrollBy(0,1300)");
		click("//a[@class='ld-primary-color']");
		Reporter.log("TC11- clicking on back to lesson button");
		
		js.executeScript("window.scrollBy(0,1300)");
		
		click("//span[contains(text(),'Basic Investment & Social Media Influencing')]");
		Reporter.log("TC11- clicking on Basic Investment & Social Media Influencing");
		
		js.executeScript("window.scrollBy(0,1300)");
		click("//div[@id='learndash_post_183']//input[@class='learndash_mark_complete_button']");
		
		js.executeScript("window.scrollBy(0,1300)");
		click("//div[@id='learndash_post_83']//input[@class='learndash_mark_complete_button']");
		Reporter.log("TC11- clicking on Mark Complete");
		
		js.executeScript("window.scrollBy(0,600)");
		click("//span[contains(text(),'Success with Advert')]");
		
		js.executeScript("window.scrollBy(0,100)");
		click("//div[@id='learndash_post_200']//input[@class='learndash_mark_complete_button']");
		Reporter.log("TC11- clicking on Mark Complete");
		
		click("//div[@id='learndash_post_24182']//input[@class='learndash_mark_complete_button']");
		
		click("//div[@id='learndash_post_85']//input[@class='learndash_mark_complete_button']");
		
		js.executeScript("window.scrollBy(0,1200)");
		click("//div[@id='learndash_post_87']//input[@class='learndash_mark_complete_button']");
		
		
		
	}
}
