package testCase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import projectBase.ProjecBase;
import projectListeners.ExtentListeners;

@Listeners(ExtentListeners.class)
public class testActivity9 extends ProjecBase {
    
	@Parameters({"username", "password"})
	@Test
	public void testcase9 (String username, String password) {
		
		 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		 driver.manage().window().maximize();
		 JavascriptExecutor js = (JavascriptExecutor) driver;
		
		 click("//a[contains(text(),'My Account')]");
		 Reporter.log("TC9- Clicking on My account page");
		 

	     click("//a[@class='ld-login ld-login ld-login-text ld-login-button ld-button']");
	     Reporter.log("TC9- Clicking on login page");
	     
		  sendkey("//input[@id='user_login']", username);
		  sendkey("//input[@id='user_pass']", password);
	     
	     Reporter.log("TC9- Entered UserName and PASSWORD");
	   
	     click("//input[@id='wp-submit']");
	     Reporter.log("TC9- clicking on submit button");
		 
	     wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[contains(text(),'All Courses')]")));
		 click("//a[contains(text(),'All Courses')]");
		 Reporter.log("TC9- clicking on All Courses");
		 
		 js.executeScript("window.scrollBy(0,600)");
		 click("//article[@id='post-69']//a[@class='btn btn-primary'][contains(text(),'See more...')]");
		 Reporter.log("TC9 -clicking on first course");
		 
		 Assert.assertEquals(driver.getTitle(), "Social Media Marketing � Alchemy LMS");
		 Reporter.log("TC9 -verifying title of the course page");
		 
		 js.executeScript("window.scrollBy(0,1300)");
		 click("//div[@id='ld-expand-83']//div[@class='ld-item-list-item-preview']//div[@class='ld-status-icon ld-status-incomplete']");
		 
		 js.executeScript("window.scrollBy(0,1300)");
		 click("//span[contains(text(),'This is the First Topic')]");
		 
		 click("//div[@id='learndash_post_175']//input[@class='learndash_mark_complete_button']");
		 
	}
}
