package testCase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import projectBase.ProjecBase;
import projectListeners.ExtentListeners;

@Listeners(ExtentListeners.class)
public class testActivity2 extends ProjecBase {

	@Test
	public void testcase2() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[@class='uagb-ifb-title']")));

		String Actualheading = driver.findElement(By.xpath("//h1[@class='uagb-ifb-title']")).getText();
		String expectedHeading = "Learn from Industry Experts";
		Assert.assertEquals(Actualheading, expectedHeading);
		Reporter.log("TC2- verifying Heading");

	}

}
