package testCase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Reporter;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import junit.framework.Assert;
import projectBase.ProjecBase;
import projectListeners.ExtentListeners;

@Listeners(ExtentListeners.class)
public class testActivity14 extends ProjecBase {

	@Parameters({ "username", "password" })
	@Test
	public void testcase14(String username, String password) throws InterruptedException {

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		JavascriptExecutor js = (JavascriptExecutor) driver;

		
		driver.findElement(By.xpath("//a[contains(text(),'My Account')]")).sendKeys(Keys.ENTER);
		Reporter.log("TC14- Clicking on My account page");
		
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div/main/article/div/section[2]/div[2]/div[2]/div[2]/div[2]/a")).sendKeys(Keys.ENTER);
		Reporter.log("TC14- Clicking on Login Button");
		
		driver.findElement(By.xpath("//input[@id='user_login']")).sendKeys(username);
		Reporter.log("TC14- Entering user name");
		driver.findElement(By.xpath("//input[@id='user_pass']")).sendKeys(password);
		Reporter.log("TC14- Entering password");
		
		driver.findElement(By.xpath("//input[@id='wp-submit']")).sendKeys(Keys.ENTER);
		Reporter.log("TC14- clicking on enter");
		
		driver.findElement(By.xpath("/html/body/div[1]/header/div/div/div/div/div[3]/div/nav/div/ul/li[2]/a")).sendKeys(Keys.ENTER);
		Reporter.log("TC14- Clicking on all the course button");
	
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div/main/article/div/section[2]/div[2]/div/div/div/div[2]/article/div[2]/p[2]/a")).sendKeys(Keys.ENTER);
		Reporter.log("TC14- Clicking Email Marketing Strategies course");
		
		js.executeScript("window.scrollBy(0,1000)");
		Reporter.log("TC14- Scrolling down on course content page");
		
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div/main/article/div/div/div/div/div[3]/div[2]/div[1]/div/a/div[2]")).click();
		Reporter.log("TC14- Clicking on deliverablity of your Emails");
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='ld-focus-header']//input[@class='learndash_mark_complete_button']")));
		js.executeScript("window.scrollBy(0, 400)");
		Reporter.log("TC14- Scrolling down on deliverablity of your Emails lesson");
		
		driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div/div[3]/div[2]/form/input[4]")).click();
		Reporter.log("TC14- Clicking on Mark Complete");
		
		js.executeScript("window.scrollBy(0,1000)");
		Reporter.log("TC14- Scrolling down on deliverablity of your Emails lesson");
		
		driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div/div[3]/a")).sendKeys(Keys.ENTER);
		Reporter.log("TC14- Clicking on back to lesson link");
		
		js.executeScript("window.scrollBy(0,1000)");
		Reporter.log("TC14- scrolling down on course contant page");		
		
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div/main/article/div/div/div/div/div[3]/div[2]/div[2]/div/a/div[2]")).click();
		Reporter.log("TC14- Clicking on improving & designing marketing email lesson");
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='ld-focus-header']//input[@class='learndash_mark_complete_button']")));
		
		js.executeScript("window.scrollBy(0,1000)");
		Reporter.log("TC14- scrolling down on imporving & designing marketing emails lesson");

		driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div/div[3]/div[2]/form/input[4]")).sendKeys(Keys.ENTER);
		Reporter.log("TC14- clicking on Mark complete");		
		
		
		String ExpectedProgress = "100% COMPLETE";
		String Actualprogress =driver.findElement(By.xpath("//div[@class ='ld-progress-percentage ld-secondary-color']")).getText();
		
		Assert.assertEquals(ExpectedProgress, Actualprogress);
	}

}
