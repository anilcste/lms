package testCase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import projectBase.ProjecBase;
import projectListeners.ExtentListeners;

@Listeners(ExtentListeners.class)
public class testActivity12 extends ProjecBase {

	@Parameters({ "username", "password" })
	@Test
	public void testcase12(String username, String password) {

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		JavascriptExecutor js = (JavascriptExecutor) driver;

		click("//a[contains(text(),'My Account')]");
		Reporter.log("TC12- Clicking on My account page");

		click("//a[@class='ld-login ld-login ld-login-text ld-login-button ld-button']");
		Reporter.log("TC12- Clicking on login page");

		sendkey("//input[@id='user_login']", username);
		sendkey("//input[@id='user_pass']", password);
		Reporter.log("TC12- Entered UserName and PASSWORD");

		click("//input[@id='wp-submit']");
		Reporter.log("TC12- clicking on submit button");

		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[contains(text(),'All Courses')]")));
		click("//a[contains(text(),'All Courses')]");
		Reporter.log("TC12- clicking on All Courses");

		js.executeScript("window.scrollBy(0,450)");

		click("//article[@id='post-71']//a[@class='btn btn-primary']");
        Reporter.log("TC12- Clicking on Email Marketing Strategies");
		
        js.executeScript("window.scrollBy(0,1300)");

		click("//div[contains(text(),'Deliverability Of Your Emails')]");
		Reporter.log("TC12- Clicking on Deliverability Of Your Emails");
		
		js.executeScript("window.scrollBy(0,350)");
		
		click("//div[@id='learndash_post_91']//input[@class='learndash_mark_complete_button']");
		
		String courseprogress = driver.findElement(By.xpath("//div[@class='ld-progress-percentage ld-secondary-color']")).getText();
		System.out.println("Course progress is = " + courseprogress);
		Reporter.log("TC12- course progress is =" + courseprogress);
	}
}