package projectBase;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

public class ProjecBase {
	public static WebDriver driver;
	public static WebDriverWait wait;
	String timeStamp;
	File screenShotName;
	

	@BeforeClass
	public void setup() {
		driver = new FirefoxDriver();
		wait = new WebDriverWait(driver, 10);
		driver.get("https://alchemy.hguy.co/lms");
		Reporter.log("Opening Browser");
	}

	
	public void failed(String testMethodName) {
   //taking screen  shot
	File scrfile =((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
		try {
			FileUtils.copyFile(scrfile, new File("C:\\Users\\AnilPatidar\\eclipse-workspace\\projectAssignment\\test-output\\ScreenShot\\"+ testMethodName + "_"+timeStamp+ ".jpg"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	public void click(String locator) {

		driver.findElement(By.xpath(locator)).click();
	}

	public void sendkey(String locator, String value) {

		driver.findElement(By.xpath(locator)).sendKeys(value);

	}
	
	
	public void submit(String locator) {

		driver.findElement(By.xpath(locator)).submit();

	}
	
	
	public void enter(String locator) {

		driver.findElement(By.xpath(locator)).sendKeys(Keys.ENTER);

	}
	
	
	
	public boolean isElementPresent(By by) {
		
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
		
	}
	
	
	@AfterClass
	public void tearDonw() throws InterruptedException {
	  	
		if( driver != null) {
	  		driver.close();
	  	}
		Reporter.log("Closing Broswser");
		
	}
}
