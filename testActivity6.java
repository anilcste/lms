package testCase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import projectBase.ProjecBase;
import projectListeners.ExtentListeners;

@Listeners(ExtentListeners.class)
public class testActivity6 extends ProjecBase {

	@Test
	public void testcase6() {

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		click("//a[contains(text(),'My Account')]");
		Reporter.log("TC6- Clicking on My account page");

		String ActualTitle = driver.getTitle();
		String ExpectedTitle = "My Account � Alchemy LMS";
		Assert.assertEquals(ActualTitle, ExpectedTitle);
		click("//a[@class='ld-login ld-login ld-login-text ld-login-button ld-button']");
		Reporter.log("TC6- Clicking on login page");

		sendkey("//input[@id='user_login']", "root");
		sendkey("//input[@id='user_pass']", "pa$$w0rd");
		Reporter.log("TC6- Entered UserName and PASSWORD");
		click("//input[@id='wp-submit']");
		Reporter.log("TC6- clicking on submit button");

		Assert.assertTrue(isElementPresent(
		By.xpath("//a[@class='ld-logout ld-logout ld-login-text ld-login-button ld-button']")));
		Reporter.log("TC6- checking if user is logged in");

	}

}
