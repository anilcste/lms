package testCase;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import projectBase.ProjecBase;
import projectListeners.ExtentListeners;

@Listeners(ExtentListeners.class)
public class testActivity5 extends ProjecBase {

	@Test
	public void testcase5() {

		click("//a[contains(text(),'My Account')]");
		String ActualTitle = driver.getTitle();
		String ExpectedTitle = "My Account � Alchemy LMS";
		Assert.assertEquals(ActualTitle, ExpectedTitle);
		Reporter.log("TC5- Verifing My Account page title");

	}

}
